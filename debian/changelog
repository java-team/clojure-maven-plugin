clojure-maven-plugin (1.7.1-3) UNRELEASED; urgency=normal

  [ Louis-Philippe Véronneau ]
  * d/control: Migrate to the Clojure Team.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Sat, 08 Jan 2022 17:54:00 -0500

clojure-maven-plugin (1.7.1-2) unstable; urgency=medium

  * Update Vcs-* links and declare compliance with S-V 4.1.4.

 -- Elana Hashman <ehashman@debian.org>  Sun, 24 Jun 2018 20:03:34 -0400

clojure-maven-plugin (1.7.1-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version to 4.1.3.
  * Watch file points upstream rather than to githubredir.
  * Remove Peter Collingbourne from uploaders and add myself.

 -- Elana Hashman <ehashman@debian.org>  Mon, 15 Jan 2018 11:25:19 -0500

clojure-maven-plugin (1.3.3-4) unstable; urgency=medium

  * Team upload

  [ tony mancill ]
  * Remove Ramakrishnan Muthukrishnan from Uploaders (Closes: #859288)

  [ Emmanuel Bourg ]
  * Depend on libmaven3-core-java instead of libmaven2-core-java
  * Build with the DH sequencer instead of CDBS
  * Standards-Version updated to 4.0.0
  * Switch to debhelper level 10
  * Use secure Vcs-* URLs
  * Removed the deprecated DM-Upload-Allowed field

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 01 Aug 2017 11:03:54 +0200

clojure-maven-plugin (1.3.3-3) unstable; urgency=low

  * Team upload.
  * Add Build-Depends on libmaven-plugin-tools-java. (Closes: #643497).
  * Bump Standards-Version to 3.9.2. No changes were required.
  * Remove unneccesary patch.
  * Remove unneccesary Build-Depends on quilt.
  * Switch to source format 3.0 (quilt).

 -- Miguel Landaeta <miguel@miguel.cc>  Sat, 22 Oct 2011 14:25:17 -0430

clojure-maven-plugin (1.3.3-2) unstable; urgency=low

  * debian/control, debian/maven.ignoreRules: use m-r-h 1.2 maven rules
    format (Closes: #591125)
  * debian/control: update to Standards-Version 3.9.1

 -- Peter Collingbourne <peter@pcc.me.uk>  Sat, 07 Aug 2010 00:59:31 +0100

clojure-maven-plugin (1.3.3-1) unstable; urgency=low

  * New upstream release.

 -- Ramakrishnan Muthukrishnan <rkrishnan@debian.org>  Sun, 13 Jun 2010 20:50:00 +0530

clojure-maven-plugin (1.3.2-1) unstable; urgency=low

  * New upstream release (Closes: #569096)
  * debian/control: added "DM-Upload-Allowed: yes"
  * debian/control: added Ramakrishnan Muthukrishnan as an Uploader
  * debian/patches/01-pom-plexus-utils.patch: describe the patch
  * debian/control: added Vcs-Browser and Vcs-Git fields

 -- Peter Collingbourne <peter@pcc.me.uk>  Wed, 17 Mar 2010 20:22:19 +0000

clojure-maven-plugin (1.3.1+git20100222.98b8797-1) UNRELEASED; urgency=low

  * New upstream snapshot
  * debian/control, debian/patches/series, debian/rules: switch to
    patchsys-quilt to fix a bug that patches were included directly in
    the .diff.gz file
  * debian/copyright: updated to reflect current licensing position
  * debian/docs: added README.markdown

 -- Peter Collingbourne <peter@pcc.me.uk>  Tue, 23 Feb 2010 14:02:15 +0000

clojure-maven-plugin (1.3.1-1) UNRELEASED; urgency=low

  * Initial release

 -- Peter Collingbourne <peter@pcc.me.uk>  Tue, 09 Feb 2010 13:28:36 +0000
